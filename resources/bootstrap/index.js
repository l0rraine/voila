import store from '../store'
import '../interceptors/axios'

export default {
  install (Vue, options) {
    // In this way we'll expose our class in all Vue components
    Vue.showSnack = function (msg, timeout) { store.commit('snackbar/setSnack', { message: msg, timeout: timeout }) }
    Vue.prototype.$snack = function (msg, timeout) { store.commit('snackbar/setSnack', { message: msg, timeout: timeout }) }
    Vue.mixin({

    })
  }
}
