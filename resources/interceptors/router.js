import router from '../router'
import util from '../libs/util'

router.beforeEach((to, from, next) => {
  util.title(to.meta.title)
  util.toDefaultPage(router, to.name, router, next)
  next()
})

router.afterEach((to) => {
  // Util.openNewPage(router.app, to.name, to.params, to.query)
  window.scrollTo(0, 0)
})
