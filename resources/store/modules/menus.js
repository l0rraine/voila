export default {
  namespaced: true,
  state: {
    menuList: []
  },
  mutations: {
    // 接受前台数组，刷新菜单
    updateMenulist (state, menus) {
      state.menuList = menus
    }
  }
}
