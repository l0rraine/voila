export default {
  namespaced: true,
  state: {
    message: '',
    timeout: ''
  },
  mutations: {
    setSnack (state, snack) {
      state.message = snack.message
      state.timeout = snack.timeout || 3000
    }
  }
}
