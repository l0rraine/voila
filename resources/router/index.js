import Vue from 'vue'
import VueRouter from 'vue-router'

import util from '../libs/util'

import { routers } from './router'

Vue.use(VueRouter)

// 路由配置
const RouterConfig = {
  hashbang: false,
  linkActiveClass: 'active',
  base: util.getRouterBase(),
  // mode: 'history',
  routes: routers
}

export const router = new VueRouter(RouterConfig)
