<?php

namespace Voila\AdminPanel;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Spatie\Cors\Cors;
use Voila\AdminPanel\app\Exceptions\CustomHandler;
use Voila\AdminPanel\app\Http\Middleware\Authenticate;
use Voila\AdminPanel\app\Http\Middleware\SetAdminReferer;

class VoilaServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        app('router')->aliasMiddleware('admin.referer', SetAdminReferer::class);
        app('router')->aliasMiddleware('admin.auth', Authenticate::class);

        $this->publishes([
                             __DIR__ . '/config'   => config_path('/'),
                             __DIR__ . '/database' => base_path('/database'),

                         ], 'voila-adminpanel');

        $this->publishes([
                             __DIR__ . '/../vue.config.laravel.js'   => base_path('vue.config.js'),
                             __DIR__ . '/../babel.config.js'   => base_path('babel.config.js'),
                             __DIR__ . '/../package.json' => base_path('package.json'),
                         ],'voila-node');

        $this->mergeConfigFrom(__DIR__ . '/config/voila.php', 'voila');


        $this->loadViewsFrom(resource_path('views/vendor/voila/'), 'voila.adminpanel');


        $this->app->bind(
            ExceptionHandler::class,
            CustomHandler::class
        );

    }

    function mergeConfigKey($source, $target)
    {
        $config = \Config::get($source, []);

        \Config::set($target, array_merge($config, \Config::get($target, [])));

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->setupRoutes();

//        app('router')->aliasMiddleware('voila.referer', SetAdminReferer::class);
        app('router')->aliasMiddleware('voila.auth', Authenticate::class);
        app('router')->aliasMiddleware('cors',Cors::class);

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Voila', "Voila\\AdminPanel\\VoilaServiceProvider");


//        $this->app->register(\Spatie\Permission\PermissionServiceProvider::class);
    }


    private function setupRoutes()
    {
        require __DIR__ . '/routes/auth.php';


        Route::get(config('voila.url_prefix'), 'Voila\AdminPanel\app\Http\Controllers\AdminPanelController@getIndex')->name('voila.home');


        $middleware = ['api','jwt.auth'];

        Route::group([
                         'middleware' => $middleware,
                         'prefix'     => config('voila.url_prefix')],
            function () {
                Route::group(['namespace' => '\Voila\AdminPanel\app\Http\Controllers'], function () {
                    Route::get('/baseconfig', 'AdminPanelController@getBaseConfig')->name('voila.config.base');
                    Route::get('/get_menus', 'AdminPanelController@getMenus')->name('voila.config.menu');


                });
                //路由
//                require __DIR__ . '/routes/routes.php';

            });

    }


}
